getD <- function (x, ...) {
  UseMethod("getD", x)
}

getD.bdat <- function(x, ...){
  rBDAT::getDiameter(x, Hx=1.3, bark=TRUE)
}

df <- rBDAT::buildTree(list(spp=1, D1=30, H=27))
class(df) <- "bdat"
class(df)

getD(df)
