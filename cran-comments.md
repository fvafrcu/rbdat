Dear CRAN Team,
this is a resubmission of package 'rBDAT'. I have added the following changes:

* FIXME

Please upload to CRAN.
Best, Christian

# Package rBDAT 0.9.7.9000

Reporting is done by packager version 1.10.0.9004


## Test environments
- R Under development (unstable) (2021-01-20 r79849)
   Platform: x86_64-pc-linux-gnu (64-bit)
   Running under: Devuan GNU/Linux 3 (beowulf)
   0 errors | 0 warnings | 1 note 
- win-builder (devel)

## Local test results
- RUnit:
    rBDAT_unit_test - 1 test function, 0 errors, 0 failures in 1 checks.
- Testthat:
    
- Coverage by covr:
    rBDAT Coverage: 33.84%

## Local meta results
- Cyclocomp:
     Exceeding maximum cyclomatic complexity of 10 for buildTree by 90.
     Exceeding maximum cyclomatic complexity of 10 for plot.datBDAT by 6.
     Exceeding maximum cyclomatic complexity of 10 for getSpeciesCode by 3.
     Exceeding maximum cyclomatic complexity of 10 for getVolume.datBDAT by 1.
- lintr:
    found 290 lints in 2624 lines of code (a ratio of 0.1105).
- cleanr:
    found 36 dreadful things about your code.
- codetools::checkUsagePackage:
    found 83 issues.
- devtools::spell_check:
    found 173 unkown words.
